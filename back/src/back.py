from flask import Flask
from flask_cors import CORS

app = Flask(__name__)
CORS(app, supports_credentials=True)

@app.route('/method', methods = ['GET', 'POST'])
def post_method():

    result = "result"

    return result